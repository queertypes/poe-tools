-----------------------------------------------------------------------------
-- |
-- Module      :  Main (poe-tools-hlint)
-- Copyright   :  Copyright (C) 2017 Allele Dev
-- License     :  GPL-3 (see the file LICENSE)
-- Maintainer  :  allele.dev@gmail.com
-- Stability   :  provisional
-- Portability :  portable
--
-- This module runs HLint on the poe-tools source tree.
--
-- Adapted from the lens project:
-- - https://github.com/ekmett/lens/blob/master/tests/hlint.hs
-----------------------------------------------------------------------------
module Main where

import Control.Monad
import Language.Haskell.HLint
import System.Environment
import System.Exit

main :: IO ()
main = do
  args <- getArgs
  hints <- hlint $ ["src", "--cpp-define=HLINT", "--cpp-ansi"] ++ args
  unless (null hints) exitFailure
