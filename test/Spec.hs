-----------------------------------------------------------------------------
-- |
-- Module      :  Main (poe-tools-spec)
-- Copyright   :  Copyright (C) 2017 Allele Dev
-- License     :  GPL-3 (see the file LICENSE)
-- Maintainer  :  allele.dev@gmail.com
-- Stability   :  provisional
-- Portability :  portable
--
-- This is the primary test suite.
-----------------------------------------------------------------------------
module Main where

import YNotPrelude

import Test.Tasty
import Test.Tasty.QuickCheck
import Test.Tasty.Hspec

main :: IO ()
main = putStrLn "Test suite not yet implemented"
