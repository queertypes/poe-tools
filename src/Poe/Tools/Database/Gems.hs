{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS -Wall #-}
module Poe.Tools.Database.Gems where

import Data.Profunctor.Product.TH
import Data.UUID (UUID)
import Database.PostgreSQL.Simple.FromField hiding (name)
import Database.PostgreSQL.Simple.ToField
import Opaleye
import YNotPrelude hiding (optional)
import qualified Data.ByteString.Char8 as BC

data PoeVersion
  = PV2_6_0

instance Show PoeVersion where
  show PV2_6_0 = "2.6.0"

instance FromField PoeVersion where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "2_6_0") -> pure PV2_6_0
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField PoeVersion where
  toField PV2_6_0 = Plain "2_6_0"

data CharacterClass
  = Marauder
  | Ranger
  | Witch
  | Duelist
  | Templar
  | Shadow
  | Scion
  deriving Show

instance FromField CharacterClass where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "marauder") -> pure Marauder
    (Just "ranger") -> pure Ranger
    (Just "witch") -> pure Witch
    (Just "duelist") -> pure Duelist
    (Just "templar") -> pure Templar
    (Just "shadow") -> pure Shadow
    (Just "scion") -> pure Scion
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField CharacterClass where
  toField Marauder = Plain "marauder"
  toField Ranger = Plain "marauder"
  toField Witch = Plain "witch"
  toField Duelist = Plain "duelist"
  toField Templar = Plain "templar"
  toField Shadow = Plain "shadow"
  toField Scion = Plain "scion"

data Difficulty
  = Normal | Cruel | Merciless
  deriving Show

instance FromField Difficulty where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "normal") -> pure Normal
    (Just "cruel") -> pure Cruel
    (Just "merciless") -> pure Merciless
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField Difficulty where
  toField Normal = Plain "normal"
  toField Cruel = Plain "cruel"
  toField Merciless = Plain "merciless"

instance QueryRunnerColumnDefault PGText Difficulty where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

data GemColor
  = Red | Green | Blue | Colorless
  deriving Show

instance FromField GemColor where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "red") -> pure Red
    (Just "green") -> pure Green
    (Just "blue") -> pure Blue
    (Just "colorless") -> pure Colorless
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField GemColor where
  toField Red = Plain "red"
  toField Green = Plain "green"
  toField Blue = Plain "blue"
  toField Colorless = Plain "colorless"

instance QueryRunnerColumnDefault PGText GemColor where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

data GemQuest
  = DyingExile
  | EnemyAtTheGate
  | CagedBrute
  | SirensCadence
  | BreakingSomeEggs
  | SharpAndCruel
  | IntrudersInBlack
  | LostInLove
  | FixtureOfFate
  | SeverTheRightHand
  | EternalNightmare
  | BreakingTheSeal
  deriving Show

instance FromField GemQuest where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "dying_exile") -> pure DyingExile
    (Just "enemy_at_the_gate") -> pure EnemyAtTheGate
    (Just "caged_brute") -> pure CagedBrute
    (Just "sirens_cadence") -> pure SirensCadence
    (Just "breaking_some_eggs") -> pure BreakingSomeEggs
    (Just "sharp_and_cruel") -> pure SharpAndCruel
    (Just "intruders_in_black") -> pure IntrudersInBlack
    (Just "lost_in_love") -> pure LostInLove
    (Just "fixture_of_fate") -> pure FixtureOfFate
    (Just "sever_the_right_hand") -> pure SeverTheRightHand
    (Just "eternal_nightmare") -> pure EternalNightmare
    (Just "breaking_the_seal") -> pure BreakingTheSeal
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField GemQuest where
  toField DyingExile = Plain "dying_exile"
  toField EnemyAtTheGate = Plain "enemy_at_the_gate"
  toField CagedBrute = Plain "caged_brute"
  toField SirensCadence = Plain "sirens_cadence"
  toField BreakingSomeEggs = Plain "breaking_some_eggs"
  toField SharpAndCruel = Plain "sharp_and_cruel"
  toField IntrudersInBlack = Plain "intruders_in_black"
  toField LostInLove = Plain "lost_in_love"
  toField FixtureOfFate = Plain "fixture_of_fate"
  toField SeverTheRightHand = Plain "sever_the_right_hand"
  toField EternalNightmare = Plain "eternal_nightmare"
  toField BreakingTheSeal = Plain "breaking_the_seal"


instance QueryRunnerColumnDefault PGText GemQuest where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

data GemType
  = ActiveGem | SupportGem
  deriving Show

instance FromField GemType where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "active") -> pure ActiveGem
    (Just "support") -> pure SupportGem
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField GemType where
  toField ActiveGem = Plain "active"
  toField SupportGem = Plain "support"

instance QueryRunnerColumnDefault PGText GemType where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

data GemTag
  = Aoe
  | Attack
  | Aura
  | Bow
  | Cast
  | Chaining
  | Chaos
  | Cold
  | Curse
  | Duration
  | Fire
  | Golem
  | Lightning
  | Melee
  | Mine
  | Minion
  | Movement
  | Projectile
  | Spell
  | Support
  | Totem
  | Trap
  | Trigger
  | Vaal
  | WarCry
  deriving Show

instance FromField GemTag where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "aoe") -> pure Aoe
    (Just "attack") -> pure Attack
    (Just "aura") -> pure Aura
    (Just "bow") -> pure Bow
    (Just "cast") -> pure Cast
    (Just "chaining") -> pure Chaining
    (Just "chaos") -> pure Chaos
    (Just "cold") -> pure Cold
    (Just "curse") -> pure Curse
    (Just "duration") -> pure Duration
    (Just "fire") -> pure Fire
    (Just "golem") -> pure Golem
    (Just "lightning") -> pure Lightning
    (Just "melee") -> pure Melee
    (Just "mine") -> pure Mine
    (Just "minion") -> pure Minion
    (Just "movement") -> pure Movement
    (Just "projectile") -> pure Projectile
    (Just "spell") -> pure Spell
    (Just "support") -> pure Support
    (Just "totem") -> pure Totem
    (Just "trap") -> pure Trap
    (Just "trigger") -> pure Trigger
    (Just "vaal") -> pure Vaal
    (Just "warcry") -> pure WarCry
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField GemTag where
  toField Aoe = Plain "aoe"
  toField Attack = Plain "attack"
  toField Aura = Plain "aura"
  toField Bow = Plain "bow"
  toField Cast = Plain "cast"
  toField Chaining = Plain "chaining"
  toField Chaos = Plain "chaos"
  toField Cold = Plain "cold"
  toField Curse = Plain "curse"
  toField Duration = Plain "duration"
  toField Fire = Plain "fire"
  toField Golem = Plain "golem"
  toField Lightning = Plain "lightning"
  toField Melee = Plain "melee"
  toField Mine = Plain "mine"
  toField Minion = Plain "minion"
  toField Movement = Plain "movement"
  toField Projectile = Plain "projectile"
  toField Spell = Plain "spell"
  toField Support = Plain "support"
  toField Totem = Plain "totem"
  toField Trap = Plain "trap"
  toField Trigger = Plain "trigger"
  toField Vaal = Plain "vaal"
  toField WarCry = Plain "warcry"


instance QueryRunnerColumnDefault PGText GemTag where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

data GemTrigger
  = OnBlock
  | WhenDamageTakenWithChance
  | OnCriticalStrike
  | OnDeath
  | WhenStunned
  | CurseOnHit
  | OnMeleeKill
  | WhenDamageTakenWithAmount
  deriving Show

instance FromField GemTrigger where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "on_block") -> pure OnBlock
    (Just "when_damage_taken_with_chance") -> pure WhenDamageTakenWithChance
    (Just "on_critical_strike") -> pure OnCriticalStrike
    (Just "on_death") -> pure OnDeath
    (Just "when_stunned") -> pure WhenStunned
    (Just "curse_on_hit") -> pure CurseOnHit
    (Just "on_melee_kill") -> pure OnMeleeKill
    (Just "when_damage_taken_with_amoun") -> pure WhenDamageTakenWithAmount
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField GemTrigger where
  toField OnBlock = Plain "on_block"
  toField WhenDamageTakenWithChance = Plain "when_damage_taken_with_chance"
  toField OnCriticalStrike = Plain "on_critical_strike"
  toField OnDeath = Plain "on_death"
  toField WhenStunned = Plain "when_stunned"
  toField CurseOnHit = Plain "curse_on_hit"
  toField OnMeleeKill = Plain "on_melee_kill"
  toField WhenDamageTakenWithAmount = Plain "when_damage_taken_with_amoun"


instance QueryRunnerColumnDefault PGText GemTrigger where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

data Stat
  = Strength | Intelligence | Dexterity
  deriving Show

instance FromField Stat where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "str") -> pure Strength
    (Just "dex") -> pure Dexterity
    (Just "int") -> pure Intelligence
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField Stat where
  toField Strength = Plain "str"
  toField Dexterity = Plain "dex"
  toField Intelligence = Plain "int"

instance QueryRunnerColumnDefault PGText Stat where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

data ManaCostType
  = Immediate | Reserved
  deriving Show

instance FromField ManaCostType where
  fromField f mdata = case BC.unpack <$> mdata of
    (Just "immediate") -> pure Immediate
    (Just "reserved") -> pure Reserved
    (Just x) -> returnError ConversionFailed f x
    Nothing -> returnError UnexpectedNull f ""

instance ToField ManaCostType where
  toField Immediate = Plain "immediate"
  toField Reserved = Plain "reserved"

instance QueryRunnerColumnDefault PGText ManaCostType where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

data Gem' a b c d e f g h i =
  Gem { id :: a
      , version :: b
      , gemType :: c
      , name :: d
      , color :: e
      , mainStat :: f
      , secondaryStat :: g
      , levelRequired :: h
      , tags :: i
      }



type Gem = Gem' UUID PoeVersion GemType Text GemColor Stat (Maybe Stat) Word [GemTag]

type GemSQL =
  Gem'
    (Column PGUuid)
    (Column PGText)
    (Column PGText)
    (Column PGText)
    (Column PGText)
    (Column PGText)
    (Column (Nullable PGText))
    (Column PGInt4)
    (Column (PGArray PGText))

$(makeAdaptorAndInstance "pGems" ''Gem')


gemsTable :: Table GemSQL GemSQL
gemsTable =
  Table "gems" (pGems Gem { id = required "gem_id"
                          , version = required "version"
                          , gemType = required "gem_type"
                          , name = required "gem_name"
                          , color = required "color"
                          , mainStat = required "main_stat"
                          , secondaryStat = required "secondary_stat"
                          , levelRequired = required "level_required"
                          , tags = required "tags"
                          })
