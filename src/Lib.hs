-----------------------------------------------------------------------------
-- |
-- Module      :  Lib
-- Copyright   :  Copyright (C) 2017 Allele Dev
-- License     :  GPL-3 (see the file LICENSE)
-- Maintainer  :  allele.dev@gmail.com
-- Stability   :  provisional
-- Portability :  portable
-----------------------------------------------------------------------------
module Lib () where

import YNotPrelude
