-----------------------------------------------------------------------------
-- |
-- Module      :  Main (poe-tools-benchmark)
-- Copyright   :  Copyright (C) 2017 Allele Dev
-- License     :  GPL-3 (see the file LICENSE)
-- Maintainer  :  allele.dev@gmail.com
-- Stability   :  provisional
-- Portability :  portable
--
-- This is the core benchmark.
-----------------------------------------------------------------------------
module Main where

import YNotPrelude

import Criterion.Main

main :: IO ()
main = putStrLn "Benchmark not yet implemented"
