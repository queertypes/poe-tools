-- ================================================================================ --
--                       Path of Exile: 0001                                        --
--                               Gems                                               --
-- ================================================================================ --

CREATE EXTENSION IF NOT EXISTS 'uuid-ossp';

--------------------------------------------------------------------------------
--                              Types                                         --
--------------------------------------------------------------------------------
CREATE TYPE poe_version AS ENUM (
  '2_6_0'
)

CREATE TYPE character_class AS ENUM (
  'marauder',
  'ranger',
  'witch',
  'duelist',
  'templar',
  'shadow',
  'scion'
)

CREATE TYPE difficulty AS ENUM (
  'normal',
  'cruel',
  'merciless'
)

-- used for gem tier unlocks
CREATE TYPE gem_quest AS ENUM (
  'dying_exile',
  'enemy_at_the_gate',
  'caged_brute',
  'sirens_cadence',
  'breaking_some_eggs',
  'sharp_and_cruel',
  'intruders_in_black',
  'lost_in_love',
  'fixture_of_fate',
  'sever_the_right_hand',
  'eternal_nightmare',
  'breaking_the_seal'
)

CREATE TYPE gem_color AS ENUM (
  'green',
  'red',
  'blue',
  'colorless'
)

CREATE TYPE gem_type AS ENUM (
  'active',
  'support'
)

CREATE TYPE gem_tag AS ENUM (
  'aoe',
  'attack',
  'aura',
  'bow',
  'cast',
  'chaining',
  'chaos',
  'cold',
  'curse',
  'duration',
  'fire',
  'golem',
  'lightning',
  'melee',
  'mine',
  'minion',
  'movement',
  'projectile',
  'spell',
  'support',
  'totem',
  'trap',
  'trigger',
  'vaal',
  'warcry'
)

CREATE TYPE gem_trigger AS ENUM (
  'on_block',
  'when_damage_taken_with_chance',
  'on_critical_strike',
  'on_death',
  'when_stunned',
  'curse_on_hit',
  'on_melee_kill',
  'when_damage_taken_with_amount'
)

CREATE TYPE stat AS ENUM (
  'str',
  'int',
  'dex'
)

CREATE TYPE mana_cost_type AS ENUM (
  'immediate',
  'reserved'
)

--------------------------------------------------------------------------------
--                              Tables                                        --
--------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS gems (
  gem_id uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  version poe_version NOT NULL DEFAULT '2_6_0',
  gem_type gem_type NOT NULL,
  gem_name text NOT NULL,
  color gem_color NOT NULL,
  main_stat_type stat NOT NULL,
  secondary_stat_type stat,
  level_required int NOT NULL,
  tags gem_tag[] NOT NULL,
  UNIQUE (gem_name, version)
)

CREATE TABLE IF NOT EXISTS active_gems (
  gem_name text PRIMARY KEY REFERENCES gems(gem_name) ON DELETE CASCADE,
  mana_cost_type mana_cost_type NOT NULL,
  cooldown_millisecs int NOT NULL,
  cast_time_millisecs int NOT NULL,
  duration_millisecs int NOT NULL
)

CREATE TABLE IF NOT EXISTS active_gem_with_immediate_cost (
  gem_name text PRIMARY KEY REFERENCES gems(gem_name) ON DELETE CASCADE,
  mana_cost int NOT NULL
)

CREATE TABLE IF NOT EXISTS active_gems_with_reserved_cost (
  gem_name text PRIMARY KEY REFERENCES gems(gem_name) ON DELETE CASCADE,
  reserved_mana_percent int NOT NULL
)

CREATE TABLE IF NOT EXISTS support_gems (
  gem_name text PRIMARY KEY REFERENCES gems(gem_name) ON DELETE CASCADE,
  mana_cost_multiplier int NOT NULL
)

CREATE TABLE IF NOT EXISTS gem_stat_requirements (
  gem_name text PRIMARY KEY REFERENCES gems(gem_name) ON DELETE CASCADE,
  gem_level int NOT NULL,
  main_stat_required int NOT NULL,
  secondary_stat_required int,
  UNIQUE (gem_name, gem_level)
)

CREATE TABLE IF NOT EXISTS gem_unlocks (
  gem_name text PRIMARY KEY REFERENCES gems(gem_name) ON DELETE CASCADE,
  quest gem_quest NOT NULL,
  char_class character_class NOT NULL,
  difficulty difficulty NOT NULL,
  UNIQUE (gem_name, char_class)
)

INSERT INTO active_gems VALUES
  ('abyssal_cry', 'immediate', 4000, 1000, 4000);

-- red active gems
INSERT INTO gems (gem_type, gem_name, color, main_stat_type, secondary_stat_type, level_required, tags) VALUES
  ('active', 'abyssal_cry', 'red', 'str', null, 34, '{"warcry", "aoe", "duration", "chaos"}'),
  ('active', 'ancestral_protector', 'red', 'str', null, 4, '{"totem", "attack", "duration", "melee"}'),
  ('active', 'ancestral_warchief', 'red', 'str', null, 28, '{"totem", "attack", "duration", "aoe", "melee"}'),
  ('active', 'anger', 'red', 'str', 'int', 24, '{"aura", "spell", "aoe", "fire"}'),
  ('active', 'animate_guardian', 'red', 'str', 'int', 28, '{"spell", "minion"}'),
  ('active', 'cleave', 'red', 'str', 'dex', 1, '{"attack", "aoe", "melee"}'),
  ('active', 'decoy_totem', 'red', 'str', null, 4, '{"totem", "spell", "duration", "aoe"}'),
  ('active', 'determination', 'red', 'str', null, 24, '{"aura", "spell", "aoe"}'),
  ('active', 'devouring_totem', 'red', 'str', null, 4, '{"totem", "spell", "duration"}'),
  ('active', 'dominating_blow', 'red', 'str', 'int', 28, '{"attack", "minion", "duration", "melee"}'),
  ('active', 'earthquake', 'red', 'str', null, 28, '{"attack", "aoe", "duration", "melee"}'),
  ('active', 'enduring_cry', 'red', 'str', null, 16, '{"warcry", "aoe", "duration"}'),
  ('active', 'flame_totem', 'red', 'str', 'int', 4, '{"projectile", "totem", "spell", "duration", "fire"}'),
  ('active', 'glacial_hammer', 'red', 'str', null, 1, '{"attack", "melee", "cold"}'),
  ('active', 'ground_slam', 'red', 'str', null, 1, '{"attack", "aoe", "melee"}'),
  ('active', 'heavy_strike', 'red', 'str', null, 1, '{"attack", "melee"}'),
  ('active', 'herald_of_ash', 'red', 'str', 'int', 16, '{"spell", "aoe", "fire"}'),
  ('active', 'ice_crash', 'red', 'str', 'int', 28, '{"attack", "aoe", "cold", "melee"}'),
  ('active', 'immortal_call', 'red', 'str', null, 34, '{"spell", "duration"}'),
  ('active', 'infernal_blow', 'red', 'str', null, 1, '{"attack", "aoe", "melee", "fire"}'),
  ('active', 'leap_slam', 'red', 'str', null, 10, '{"attack", "aoe", "movement", "melee"}'),
  ('active', 'molten_shell', 'red', 'str', null, 4, '{"spell", "aoe", "duration", "fire"}'),
  ('active', 'molten_strike', 'red', 'str', null, 1, '{"projectile", "attack", "aoe", "melee", "fire"}'),
  ('active', 'punishment', 'red', 'str', 'int', 24, '{"curse", "spell", "aoe", "fire"}'),
  ('active', 'purity_of_fire', 'red', 'str', 'int', 24, '{"aura", "spell", "aoe", "fire"}'),
  ('active', 'rallying_cry', 'red', 'str', null, 10, '{"warcry", "aoe", "duration"}'),
  ('active', 'reckoning', 'red', 'str', null, 4, '{"trigger", "attack", "aoe", "melee"}'),
  ('active', 'rejuvenation_totem', 'red', 'str', null, 4, '{"totem", "aura", "spell", "aoe", "duration"}'),
  ('active', 'searing_bond', 'red', 'str', 'int', 12, '{"totem", "spell", "duration", "fire"}'),
  ('active', 'shield_charge', 'red', 'str', null, 10, '{"attack", "aoe", "movement", "melee"}'),
  ('active', 'shockwave_totem', 'red', 'str', null, 28, '{"totem", "spell", "aoe", "duration"}'),
  ('active', 'static_strike', 'red', 'str', 'int', 12, '{"attack", "melee", "aoe", "duration", "lightning"}'),
  ('active', 'summon_flame_golem', 'red', 'str', 'int', 34, '{"fire", "minion", "spell", "golem"}'),
  ('active', 'summon_stone_golem', 'red', 'str', 'dex', 34, '{"minion", "spell", "golem"}'),
  ('active', 'sunder', 'red', 'str', null, 12, '{"attack", "aoe", "melee"}'),
  ('active', 'sweep', 'red', 'str', null, 12, '{"attack", "aoe", "melee"}'),
  ('active', 'vengeance', 'red', 'str', null, 24, '{"trigger", "attack", "aoe", "melee"}'),
  ('active', 'vigilant_strike', 'red', 'str', 'int', 4, '{"attack", "melee"}'),
  ('active', 'vitality', 'red', 'str', null, 24, '{"aura", "spell", "aoe"}'),
  ('active', 'warlords_mark', 'red', 'str', null, 24, '{"curse", "spell", "aoe", "duration"}')
  ON CONFLICT (gem_name) DO NOTHING;

-- green active gems
INSERT INTO gems (gem_type, gem_name, color, main_stat_type, secondary_stat_type, level_required, tags) VALUES
  ('active', 'animate_weapon', 'green', 'dex', 'int', 4, '{"duration", "minion", "spell"}'),
  ('active', 'arctic_armor', 'green', 'dex', 'int', 16, '{"spell", "duration", "cold"}'),
  ('active', 'barrage', 'green', 'dex', null, 12, '{"attack", "bow"}'),
  ('active', 'bear_trap', 'green', 'dex', null, 4, '{"trap", "duration", "cast"}'),
  ('active', 'blade_flurry', 'green', 'dex', 'int', 28, '{}'),
  ('active', 'blade_vortex', 'green', 'dex', null, 12, '{"spell", "aoe", "duration"}'),
  ('active', 'bladefall', 'green', 'dex', null, 28, '{"spell", "aoe"}'),
  ('active', 'blast_rain', 'green', 'dex', null, 28, '{"fire", "attack", "aoe", "bow"}'),
  ('active', 'blink_arrow', 'green', 'dex', null, 10, '{"attack", "minion", "duration", "movement", "bow"}'),
  ('active', 'blood_rage', 'green', 'dex', null, 16, '{"spell", "duration"}'),
  ('active', 'burning_arrow', 'green', 'dex', null, 1, '{"attack", "fire", "bow"}'),
  ('active', 'caustic_arrow', 'green', 'dex', null, 4, '{"attack", "aoe", "duration", "chaos", "bow"}'),
  ('active', 'cyclone', 'green', 'dex', 'str', 28, '{"attack", "aoe", "movement", "melee"}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}'),
  ('active', _, 'green', 'dex', _, _, '{}')
  ON CONFLICT (gem_name) DO NOTHING;

-- blue active gems
INSERT INTO gems (gem_type, gem_name, color, main_stat_type, secondary_stat_type, level_required, acquisition_level, tags) VALUES
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  (),
  ()
  ON CONFLICT (gem_name) DO NOTHING;
