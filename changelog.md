# 0.2.0.0 (March 15, 2017)

* add initial Haskell/SQL layer, including gems table

# 0.1.0.0 (March 15, 2017)

* get things started
