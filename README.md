# poe-tools

| poe-tools   | 0.2.0.0                            |
| ----------- | ---------------------------------- |
| Maintainer  | Allele Dev (allele.dev@gmail.com ) |
| Funding     | $0 USD                             |
| Copyright   | Copyright (C) 2017 Allele Dev      |
| License     | GPL-3                              |

A small suite of tools dedicated to making it easier to play Path of
Exile. All tools are intended to be used without access to the
internet, but do require a postgresql database running locally.

## Features

## Contributing

Contributions are welcome! Documentation, examples, code, and
feedback - they all help.

Be sure to review the included code of conduct. This project adheres
to the [Contributor's Covenant](http://contributor-covenant.org/). By
participating in this project you agree to abide by its terms.

This project currently has no funding, so it is maintained strictly on
the basis of its use to me. No guarantees are made about attention to
issues or contributions, or timeliness thereof.

## Developer Setup

The easiest way to start contributing is to install
[stack](https://github.com/commercialhaskell/stack). stack can install
GHC/Haskell for you, and automates common developer tasks.

The key commands are:

* `stack setup`: install GHC
* `stack build`: build the project
* `stack clean`: clean build artifacts
* `stack haddock`: builds documentation
* `stack test`: run all tests
* `stack bench`: run all benchmarks
* `stack ghci`: start a REPL instance

## Licensing

This project is distributed under the GPL-3 license. See the included
[LICENSE](./LICENSE) file for more details.
